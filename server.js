// зависимости
const http = require('http')
const fs = require('fs')

http
    .createServer(function (request, response) {
        console.log(`Запрошенный адрес: ${request.url}`)
        if (request.url.startsWith('/')) {
            // получение пути после слеша
            let filePath = request.url.substr(1)
            console.log(`filePath: "${filePath}"`)
            // если пустая строка отдача index.html
            if (filePath == "") { filePath = "index.html" }
            console.log(`filePath: "${filePath}"`)
            // если нет ресурса
            fs.readFile(filePath, function (error, data) {
                if (error) {
                    response.statusCode = 404
                    response.end('Resourse not found!')
                } else {
            //запрошеный файл
                    response.setHeader('Content-Type', 'text/html')
                    response.end(data)
                }
            })
        } else {
            // во всех остальных случаях отправка строки hello world!
            response.end('Hello World!')
        }
    })
    .listen(80)
