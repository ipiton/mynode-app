FROM node:alpine
EXPOSE 80
RUN mkdir /app
WORKDIR /app

COPY server.js /app/server.js
COPY index.html /app/index.html
COPY ver.html /app/ver.html

CMD node /app/server.js
